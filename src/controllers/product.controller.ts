import Product from "../models/product.model"
import { CreateUpdateProductDto, IProduct } from "../types/product";

export default class ProductController {
    static async getAll(): Promise<IProduct[]> {
        return Product.find({});
    }

    static async getOne(id: string): Promise<IProduct | null> {
        return Product.findById(id);
    }

    static async createOne(info: CreateUpdateProductDto): Promise<IProduct> {
        return Product.create(info);
    }

    static async updateOne(id: string, update: CreateUpdateProductDto): Promise<IProduct | null> {
        return Product.findOneAndUpdate({ _id: id }, update, {
            new: true
        });
    }

    static async deleteOne(id: string): Promise<IProduct | null> {
        return Product.findOneAndDelete({ _id: id });
    }
}