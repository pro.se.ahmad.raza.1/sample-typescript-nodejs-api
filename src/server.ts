import app from "./app";
import { connect } from "./config/db";

// connect to database
connect();

app.listen(3000);