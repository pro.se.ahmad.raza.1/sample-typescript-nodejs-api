import express, { json } from "express";
import ProductRouter from "./routes/product.router";

const app = express();
app.use(json());

app.use('/product', ProductRouter);

export default app;