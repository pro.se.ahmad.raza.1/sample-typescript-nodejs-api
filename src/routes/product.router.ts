import { Request, Response, NextFunction, Router } from "express";
import ProductController from "../controllers/product.controller";

const ProductRouter = Router();

ProductRouter.get('/', async (req: Request, res: Response) => {
    const all = await ProductController.getAll();
    res.json(all);
});

ProductRouter.get('/:id', async (req: Request, res: Response) => {
    const one = await ProductController.getOne(req.params.id);
    res.json(one);
});

ProductRouter.post('/', async (req: Request, res: Response) => {
    const created = await ProductController.createOne(req.body);
    res.json(created);
});

ProductRouter.put('/:id', async (req: Request, res: Response) => {
    const updated = await ProductController.updateOne(req.params.id, req.body);
    res.json(updated);
});

ProductRouter.delete('/:id', async (req: Request, res: Response) => {
    const deleted = await ProductController.deleteOne(req.params.id);
    res.json(deleted);
});

export default ProductRouter;