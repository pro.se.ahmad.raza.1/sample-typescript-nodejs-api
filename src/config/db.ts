import { connect as mongooseConnect } from 'mongoose';

export function connect() {
    mongooseConnect(`mongodb://localhost:27017/ts_express`, (err) => {
        if (err) {
            console.error('Error connecting to database!');
            process.exit(1);
        } else {
            console.info('Connected to database!');
        }
    });
}
