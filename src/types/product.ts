export interface IProduct {
    _id: string;
    name: string;
    price: number
    createdAt: string | Date,
    updatedAt: string | Date
};

export interface CreateUpdateProductDto {
    name: string;
    price: number
}