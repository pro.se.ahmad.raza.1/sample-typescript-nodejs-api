import { model, Schema } from "mongoose";

const ProductSchema = new Schema({
    name: String,
    price: String
}, {
    timestamps: true
});

const Product = model('product', ProductSchema);
export default Product;